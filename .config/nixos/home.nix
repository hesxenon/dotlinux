{config, pkgs, ...}:

{
  programs.home-manager.enable = true;
  home.stateVersion = "23.05";

  home.username = "hesxenon";
  home.homeDirectory = "/home/hesxenon";

  home.packages = with pkgs; [
    autojump
    discord
    element-desktop
    kdeconnect
    keepassxc
    libreoffice
    nerdfonts
    pinta
    qutebrowser-qt6
    ranger
    todoman
    vscode
    signal-desktop
    vimPlugins.packer-nvim
  ];

  programs.kitty = {
    enable = true;
    theme = "One Dark";
    settings = {
      font_family = "FiraCode nerd Font";
      disable_ligatures = "never";
      modify_font = "underline_position 2";
    };
  };

  programs.zsh = {
    enable = true;
    dirHashes = {
      projects = "$HOME/projects";
    };
    oh-my-zsh = {
      enable = true;
      theme = "agnoster";
      plugins = [ "git" "autojump" ];
    };
    shellAliases = {
      system-rebuild = "sudo $HOME/.config/nixos/system-rebuild.sh";
      vim = "nvim";
      system-edit = "vim ~/.config/nixos";
    };
  };

  programs.neovim = {
    enable = true;
    defaultEditor = true;
  };

  services.dunst = {
    enable = true;
  };
}
