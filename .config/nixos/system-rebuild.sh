#!/bin/sh
set -e
nixos-rebuild switch --flake /home/hesxenon/.config/nixos#
git add /home/hesxenon/.config/nixos
git commit -m "build: rebuild nixos config"
