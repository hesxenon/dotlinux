-- every spec file under the "plugins" directory will be loaded automatically by lazy.nvim
--
-- In your plugin files, you can:
-- * add extra plugins
-- * disable/enabled LazyVim plugins
-- * override the configuration of LazyVim plugins
return {
  -- add oxocarbon
  { "nyoom-engineering/oxocarbon.nvim" },

  -- Configure LazyVim to load gruvbox
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "oxocarbon",
      kind_filter = {
        typescript = false,
        typescriptreact = false,
      },
    },
  },

  { "folke/snacks.nvim", opts = { scroll = { enabled = false } } },

  { "akinsho/bufferline.nvim", enabled = false },

  {
    "nvim-neo-tree/neo-tree.nvim",
    opts = {
      bind_to_cwd = false,
      follow_current_file = { enabled = false },
    },
  },
  {
    "petertriho/nvim-scrollbar",
    after = "gitsigns",
    config = function()
      require("scrollbar").setup()
    end,
  },

  -- override nvim-cmp and add cmp-emoji
  {
    "hrsh7th/nvim-cmp",
    dependencies = { "hrsh7th/cmp-emoji" },
    opts = function(_, opts)
      opts.confirmation = {
        get_commit_characters = function()
          return {}
        end,
      }
      opts.completion.completeopt = "menu,menuone,noinsert,noselect"
      opts.mapping["<CR>"] = nil
      opts.mapping["<C-Space>"] = nil
      table.insert(opts.sources, { name = "emoji" })
    end,
  },

  {
    "s1n7ax/nvim-window-picker",
    name = "window-picker",
    event = "VeryLazy",
    version = "2.*",
    config = function()
      require("window-picker").setup()
    end,
  },
  {
    "mbbill/undotree",
  },
  {
    "davidmh/mdx.nvim",
    config = true,
    dependencies = { "nvim-treesitter/nvim-treesitter" },
  },
  {
    "neovim/nvim-lspconfig",
    opts = {
      inlay_hints = {
        exclude = { "ts", "tsx", "typescript", "typescriptreact" },
      },
      defaults = {
        layout_strategy = "horizontal",
        layout_config = { prompt_position = "top" },
        sorting_strategy = "ascending",
        winblend = 0,
      },
      setup = {
        tailwindcss = function()
          if not require("lspconfig").util.root_pattern("tailwind.config.js")(vim.fn.getcwd()) then
            return true
          end
        end,
        denols = function()
          if not require("lspconfig").util.root_pattern("deno.json", "deno.jsonc")(vim.fn.getcwd()) then
            return true
          end
        end,
      },
    },
  },
  -- since `vim.tbl_deep_extend`, can only merge tables and not lists, the code above
  -- would overwrite `ensure_installed` with the new value.
  -- If you'd rather extend the default config, use the code below instead:
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      -- add tsx and treesitter
      vim.list_extend(opts.ensure_installed, {
        "bash",
        "html",
        "javascript",
        "json",
        "lua",
        "markdown",
        "markdown_inline",
        "python",
        "query",
        "regex",
        "tsx",
        "typescript",
        "vim",
        "yaml",
      })
    end,
  },

  {
    "stevearc/conform.nvim",
    opts = {
      formatters_by_ft = {
        ["markdown"] = { "prettier", "markdownlint-cli2", "markdown-toc" },
        ["markdown.mdx"] = { "prettier", "markdownlint-cli2", "markdown-toc" },
      },
    },
  },
}
