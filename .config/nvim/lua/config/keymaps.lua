-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
vim.keymap.set("n", "<C-k>", '"_dd')
vim.keymap.set("n", "<C-/>", "<leader>fT", { remap = true })
vim.keymap.set("n", "<leader>e", ":Neotree dir=./<cr>")
vim.keymap.del("n", "<space><space>")
vim.keymap.set("n", "<space><space>", function()
  require("telescope.builtin").find_files({ hidden = true })
end)
vim.keymap.del("n", "<leader>E")
