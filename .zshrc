# Add deno completions to search path
if [[ ":$FPATH:" != *":/home/hesxenon/.zsh/completions:"* ]]; then export FPATH="/home/hesxenon/.zsh/completions:$FPATH"; fi
setopt extendedglob
setopt appendhistory

# export MANPATH="/usr/local/man:$MANPATH"
# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

alias vim="nvim"
alias extract="aunpack -D"
alias please="sudo"
alias amionline="ping 8.8.8.8"
alias find='rg --no-ignore --files $PWD 2>/dev/null | rg'
alias mkdir="mkdir -p"
alias files="vifm"
alias ls='eza -a --icons'
alias ll='eza -al --icons'
alias lt='eza -a --tree --level=1 --icons'

########################
# Functions
########################
function wclip() {
  cat "$@" | wl-copy
}

# hook that runs on each cd
function chpwd() {
	ls
}

function createNodeLib() {
  local dest="$1"
  if [ ! -f $dest ]; then
    mkdir -p $dest
  fi
  cd $dest
  git init
  cat <<\EOF >>.gitignore
node_modules
lib/**/*.js
EOF

  npm init -y > /dev/null
  corepack use pnpm
  pnpm i -D typescript typedoc prettier eslint @eslint/js @types/eslint__js typescript-eslint

  tmp=$(mktemp)
  jq '.exports."." = "./lib/index.js" | .type = "module" | .license = "BSD"' package.json > $tmp
  mv $tmp package.json

  echo "{}" | jq '.compilerOptions = { "strict": true, "noUncheckedIndexedAccess": true, "module": "NodeNext", "target": "ES2022" }' > tsconfig.json

  echo "{}" > .prettierrc.json

  cat <<\EOF >>eslint.config.js
// @ts-check

import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';

export default tseslint.config(
  eslint.configs.recommended,
  ...tseslint.configs.recommended,
  {
    rules: {
      "@typescript-eslint/no-namespace": "off",
      curly: "error",
      eqeqeq: ["error", "smart"],
      complexity: "error",
    },
  },
);
EOF

  mkdir lib
  touch lib/index.ts
  echo "{}" | jq '.entryPoints = ["./lib/index.ts"]' > typedoc.json

  cat <<\EOF >>.gitlab-cy.yml
default:
  image:
    name: "alpine"
    entrypoint: [""]

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages: # a predefined job that builds your pages and saves them to the specified path.
  cache:
    key: dependencies
    paths:
      - node_modules
  script:
    - apk update
    - apk add curl bash nodejs npm
    - curl -fsSL https://bun.sh/install | bash; npm i -g pnpm@9.4.0
    - pnpm i
    - pnpm run docs
  artifacts:
    paths:
      - docs
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  environment: production
  publish: docs
EOF

  npx eslint --fix .
  npx prettier -w .
  git add .
  git config user.name "Katja Potensky" && git config user.email "785327-hesxenon@users.noreply.gitlab.com"
  git commit -m "initial commit"
}

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="blinks" # set by `omz`

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(dnf emoji git autojump deno)


source $ZSH/oh-my-zsh.sh

if [ -e /home/hesxenon/.nix-profile/etc/profile.d/nix.sh ]; then . /home/hesxenon/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# bun completions
[ -s "/home/hesxenon/.bun/_bun" ] && source "/home/hesxenon/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"
export ZSH_COMPDUMP=$ZSH/cache/.zcompdump-$HOST

[ -s /home/hesxenon/.deno_completions.sh ] && source "/home/hesxenon/.deno_completions.sh"
if [ "$TMUX" = "" ]; then tmux; fi
. "/home/hesxenon/.deno/env"
# Initialize zsh completions (added by deno install script)
autoload -Uz compinit
compinit
