export PATH="$HOME/.local/bin:$PATH"
export XDG_CONFIG_HOME=~/.config
export XDG_CACHE_HOME=~/.cache
export EDITOR=$(which nvim)
export BROWSER=$(which firefox)
export TMPDIR="/tmp"

. "/home/hesxenon/.deno/env"

